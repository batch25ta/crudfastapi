from pymongo import MongoClient
from crudfastapi.models.books_model import database as db
import csv, json
import json
from bson import ObjectId

# inisiasi object
db = db()

def objId_toStr(obj):
    return str(obj["_id"])

def search_book_by_name(**params):
    data_list = []
    for book in db.show_bookname(**params):
        book["_id"] = objId_toStr(book)
        data_list.append(book)
    return data_list

def search_books():
    data_list = []
    for book in db.show_books():
        book["_id"] = objId_toStr(book)
        data_list.append(book)
    return data_list

def search_book_by_id(**params):
    result = db.show_bookid(**params)
    result["_id"] = objId_toStr(result)
    return result

def insert_book_new(**params):
    result = db.insert_book(**params)
    result["_id"] = objId_toStr(**params)
    return result

def delete_book_id(**params):
    result = db.delete_bookid(**params)
    result["_id"] = objId_toStr(**params)
    return result

