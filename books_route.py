from fastapi import APIRouter
from crudfastapi.books_api import *

# inisasi obyek router yang akan dipanggil pada main.py
router = APIRouter()

# Semua fungsi dipanggil oleh --> book_api.py >> (from books_api import *)
# Pada inputan fungsi dijelaskan tipe data input, pada contoh adalah dict --> params:dict

# by id
@router.post("/bookbyid")
async def view_search_books_by_id(params:dict):
    result = search_book_by_id(**params)
    return result

# by name
@router.post("/bookbyname")
async def view_search_books_by_name(params:dict):
    result = search_book_by_name(**params)
    return result

# all
@router.post("/books")
async def view_search_books_by_name():
    result = search_books()
    return result

@router.get("/insert")
async def insert_book_new_db(params:dict):
    result = insert_book_new(**params)
    return result

@router.delete("/deleteid")
async def delete_book_id_db(**params:dict):
    result = delete_book_id(**params)
    return result