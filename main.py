from fastapi import FastAPI
from crudfastapi.books_route import router as books_router

app =FastAPI()

# memasukkan alamat routing pada file books_route pada file utama
app.include_router(books_router)

@app.get("/")
async def read_main():
    return {"mesaage":"Hello Bigger Applications - mongoAPI!"}


