from pymongo import MongoClient
from bson.objectid import ObjectId

class database:
    def __init__(self):
        try:
            #self.nosql_db = MongoClient("mongodb://localhost:27017/")
            self.nosql_db = MongoClient()
            self.db = self.nosql_db.dbperpustakaan
            self.mongo_col = self.db['books']
            print('Database connected!')          
        
        except Exception as e:
            print(e)

    def show_books(self):
        """ result = self.mongo_col.find()
        for item in result:
            print(item) """
        # atau 
        result = self.mongo_col.find()
        return [item for item in result]

    def show_bookid(self,**params):
        query_findid={"_id":ObjectId(params["id"])}
        result = self.mongo_col.find_one(query_findid)
        return result
        

    def show_bookname(self,**params):
        query_findname = {"nama":{"$regex":"{0}".format(params["nama"]),"$options":"i"}}
        result = self.mongo_col.find(query_findname)
        return result

    def insert_book(self, document):
        self.mongo_col.insert_one(document)

    def update_bookid(self,**params):
        query_bookid = {"_id":ObjectId(params["id"])}
        query_update = {"$set":params["data"]}
        self.mongo_col.update_one(query_bookid,query_update)
        
    
    def delete_bookid(self,**params):
        query_delid = {"_id":ObjectId(params["id"])}
        self.mongo_col.delete_one(query_delid)
        

    

